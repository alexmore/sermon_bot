#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Linux I/O stream resender
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


import config
import telebot
import sys

# echo 'alias tgsend="/home/telegram/telegram_bots/tg_msg_sender.py"' >> /home/tgsender/.profile

# Пересылаем стандартный поток ввода через бота в чат из config.chat_id	
# p.s.: чтобы завершить ввод текста, нужно зажать 'ctrl + D'
def send_message_to_tg(msg):
	t_bot = telebot.TeleBot(config.bot_tocken)	
	if not msg:
		print 'Input stream is empty. Try "echo \'Hello, SermonBot!\' | tgsend"'
		return
	for chat_id in config.chat_ids:
		t_bot.send_message(chat_id, msg.decode('utf-8'))	
	
if __name__ == '__main__':
	send_message_to_tg(sys.stdin.read())