#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Notification sender
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


import logging
import config
import MySQLdb
import telebot

ACTIVE_CONN = '00000'

# Настроим формат вывода логов, пока что в консоль
logging.basicConfig(
		format=config.log_format,
		level=logging.INFO)
		
# В фреймворке есть баг, он может падать, если сервера тг будут 
# пропускать запросы, говорят, помогает TeleBot(TOKEN,threaded=False).
# TODO: Подробнее изучить этот вопрос.
t_bot = telebot.TeleBot(config.bot_tocken)		


class SyslogSender:
	def __init__(self):
		self.host = config.db_host
		self.user = config.db_user
		self.passwd = config.db_passwd
		self.db_name = config.db_name
		self.db_table = config.db_table
		self.chat_ids = config.chat_ids
		self.syslog_id = 0
		self.syslog_attrs = [
				'ID', 'CustomerID', 'ReceivedAt', 'DeviceReportedTime',
				'Facility', 'Priority', 'FromHost', 'Message',
				'NTSeverity', 'Importance', 'EventSource', 'EventUser',
				'EventCategory', 'EventID', 'EventBinaryData', 'MaxAvailable',
				'CurrUsage', 'MinUsage', 'MaxUsage', 'InfoUnitID',
				'SysLogTag', 'EventLogType', 'GenericFileName', 'SystemID']

	def connect(self):
		try:
			self.conn = MySQLdb.connect(
						host = self.host,
						user = self.user,
						passwd = self.passwd,
						db = self.db_name) 
			logging.info(u"Successfull conection to DB: {}".format(self.db_name))
		except Exception:
			logging.info( u"Error while connecting to DB: {}.".format(self.db_name))
					
	def exec_query(self):
		query_res = ()
		if self.conn.sqlstate() == ACTIVE_CONN:
			query_res = ()
			self.syslog_id = self.get_syslog_id()
			self.query = (
					u'select * from {0} '
					u'where FromHost in {1} '
						u'and Priority in {2} '
						u'and Facility in {3} '
						u'and Message rlike {4} '
						u'and ID > {5};'.format(self.db_table,
												config.syslog_hosts,
												config.syslog_severity,
												config.syslog_facility,
												config.syslog_msg_regexp,
												self.syslog_id
												))
			self.cur = self.conn.cursor()
			try:
				self.cur.execute(self.query)
				query_res = self.cur.fetchall()
			except Exception as e:
				logging.info( u"Error while executing a "
								u"query: {}".format(self.query))
			self.conn.close()
		return query_res
		
	def send_syslog(self):
		data = self.exec_query()
		if not data:
			return
		for row in data:
			out_str = u'<b>New Syslog event:</b>\n'
			#for i in range(len(row)):
			for i in range(8):
				out_str += u'{0}:  <i>{1}</i>\n'.format(self.syslog_attrs[i], 
														row[i])
			for chat_id in self.chat_ids:
				t_bot.send_message(chat_id, out_str, parse_mode='HTML')
			self.syslog_id = row[0]
		self.set_syslog_id()
		
	def get_syslog_id(self):
		with open(config.curr_syslog_id_path, 'rb') as f:
			return f.read()
	
	def set_syslog_id(self):
		with open(config.curr_syslog_id_path, 'wb') as f:
			f.write(str(self.syslog_id))
		
if __name__ == '__main__':
	sl_sender = SyslogSender()
	sl_sender.connect()
	sl_sender.send_syslog()