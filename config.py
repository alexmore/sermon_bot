# -*- coding: UTF-8 -*-

# глобальные конфиги
bot_tocken = 'TOCKEN'
hosts_path = 'hosts.json'
log_format = (
	u'[%(filename)s: 'u'%(lineno)d, %(asctime)s] '
	u'%(levelname)s: %(message)s')
HOST_ATTRS = ['username', 'port', 'protocol']

# конфиги для syslog_sender.py
syslog_hosts = "('cerberus-main.psu.ru')"
syslog_severity = tuple([x for x in range (0, 5)]) # 0 - 4
syslog_facility = tuple([x for x in range (0, 24)]) # 0 - 23
syslog_msg_regexp = "'.+'"
curr_syslog_id_path = '/home/telegram/telegram_bots/curr_syslog_id'
db_user = 'rsyslog'
db_passwd = 'PASSWORD'
db_host = 'localhost'
db_name = 'Syslog'
db_table = 'SystemEvents'
chat_id = '-172177223'