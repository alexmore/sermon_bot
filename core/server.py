# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Base back-end classes
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------

# python modules
import paramiko
import telnetlib
import json
import re
import logging
import socket
import subprocess as sp
# project modules
import config
from lib.utils import deep_getitem
from config import HOST_ATTRS

# TODO: добавление новых серверов из бота, формирование ключей через
#	    openssl, отправка их через fscp?
# TODO: класс, хранящий состояние бота, запилить
# TODO: упаковка в контейнер
# TODO: cделать нормальный ini конфиг через configparser
# TODO: использовать супервайзер вместо костыля на баше
# TODO: заготовленные скрипты ботом запускать
# TODO: рефакторинг
# TODO: Server должен работать с абстрактным провайдером!
#       Убрать из него все лишее в core/protocols

PR_SSH = '0'
PR_TELNET = '1'
PR_RSH = '2'

# Настроим формат вывода логов, пока что в консоль
logging.basicConfig(
		format=config.log_format,
		level=logging.INFO)


class HostsManager:
	def __init__(self):
		self.hosts = self.read_hosts()
		self.active_host = ''
		self.srv = Server()
	'''	
	def add_host(self,new_host,user):
		self.data[new_host] = user
		with open('hosts.json', 'wb') as j_file:
			json.dump(self.data,j_file)
	
	def edit_host(self,host,user):
		self.data[host] = user
	'''
	
	def connect_to_host(self, new_host):
		self.active_host = new_host
		host_attrs = self.get_host_attrs(self.active_host)
		if not host_attrs:
			return False
		# TODO: Когда телнет прикрутишь, проверку правильную поставь
		if self.srv.is_ssh_connection_active():
			self.srv.disconnect()
		self.srv.host = self.active_host
		self.srv.user, self.srv.port, self.srv.protocol = host_attrs
		return self.srv.connect()
		
	def check_connection_to_hosts(self):
		hosts_state_dict = dict()
		logging.info( u"Test connection for all avaliable hosts started. ")
		for host in self.hosts:
			host_state = False
			host_attrs = self.get_host_attrs(host)
			if host_attrs:
				username, port, protocol = host_attrs
				s = Server(host, username, port, protocol)
				host_state = s.connect()
			hosts_state_dict[host] = host_state
			if host_state: 
				s.disconnect()
		return hosts_state_dict

	def get_host_attrs(self, host):
		if host not in self.hosts:
			logging.info(u"Host {} not found in hosts.".format(host))
			return None

		host = self.hosts[host]
		return [deep_getitem(host, attr) for attr in HOST_ATTRS]

	@staticmethod
	def read_hosts():
		with open(config.hosts_path, 'rb') as j_file:
			data=json.load(j_file)
		return(data)
		
	
class Server:
	def __init__(self, host='', user='', port='22', protocol=PR_SSH,
					key='', prv_key_dir='./prv_keys/'):
		self.ssh = paramiko.SSHClient()
		self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		self.host = host
		self.user = user
		self.port = port
		self.protocol = protocol 
		self.key = key
		self.prv_key_dir = prv_key_dir
		# self.prv_key_dir = 'prv_keys\\' # для винды

	# TODO: Все, что связано с подключениями по разным протоколам,
	#      вынести в отдельные модули
	def SSH_connect(self):
		try:
			# организуем подключение по RSA ключу
			self.key = paramiko.RSAKey.from_private_key_file(
							'{0}{1}'.format(self.prv_key_dir, self.user))
			self.ssh.connect(hostname = self.host, 
							 username = self.user,
							 port = int(self.port),
							 pkey = self.key
							 )
		# почему-то в парамико ошибки сокетов являются дочерними от IO,
		# поэтому пришлось подключить модуль отдельный
		except socket.error as se:
			logging.info((u"Couldn't open socket to {0}:{1} (Errno: "
							u"{2})".format(self.host, self.port, se.errno)))
		except IOError:
			logging.info((u"Couldn't read private key "
							u"from {0}{1}".format(self.prv_key_dir, self.user)))
		except (paramiko.BadHostKeyException, paramiko.AuthenticationException): 
			logging.info((u"Private Key is wrong ( "
							u"from {0}{1})".format(self.prv_key_dir, self.user)))
		except Exception:
			logging.info( u"Error while connecting to {}.".format(self.host))
		return self.is_ssh_connection_active()
		
	def telnet_connect(self):
		# some telnet connection #
		return False
		
	def connect(self):
		if self.protocol == PR_SSH: 
			return self.SSH_connect()
		if self.protocol == PR_RSH:
			return True
		if self.protocol == PR_TELNET:
			return self.telnet_connect()

	def disconnect(self):
		if self.protocol == PR_SSH: 
			self.ssh.close()
		else:
			pass

	def is_ssh_connection_active(self):
		# Интересная ситуевина: если транспорт есть, то он может
		# вернуть состояние, если его нет, то он возврашает None.
		# Дак может лучше просто все вызовы SSH в обработчики 
		# исключений обернуть? p.s.: EAFP
		transport = self.ssh.get_transport()
		if transport is not None:
			return transport.is_active()
		return False
		
	def is_telnet_connection_active(self):
		pass

	###################################
	# Команды для *nix машин
	###################################
	def get_hostname(self):
		if self.protocol == PR_RSH:
			return
		hostname = ''
		output = self.execute('hostname -i')
		if output: hostname = output.read()
		return hostname
	
	def get_cpu_info(self):
		if self.protocol == PR_RSH:
			return
		cpu_info_dict = dict()
		output = self.execute('cat /proc/cpuinfo')
		if not output:
			return {}
		for line in output:
			substr_list = re.split('[\t]+',line.strip())
			# все, у чего есть значения, складываем в словарь
			if len(substr_list) > 1: 
				cpu_info_dict[substr_list[0]] = substr_list[1].strip()
		return cpu_info_dict
		
	def get_ram_info(self):
		if self.protocol == PR_RSH:
			return
		ram_info_dict = dict()
		output = self.execute('free -h') 
		if not output:
			return {}
		# читаем первую строку, там параметры + выкидываем все отступы
		params_list = re.split('[ ]+',output.readline().strip())
		# все остальное тоже парсим и складываем по вложенным словарям
		for line in output: 
			substr_list = re.split('[ ]+',line.strip())
			if substr_list[0] != '-/+':
				ram_info_dict[substr_list[0][:-1]] = dict(zip(params_list, 
															substr_list[1:]))
		return ram_info_dict
		
	def get_mem_info(self):
		if self.protocol == PR_RSH:
			return
		mem_info_list = []
		# из-за грепа параметров то нет, поэтому пришлось захардкодить
		params_list = ['Fs','Fs_type','Size','Used','Free','Used%', 'Mount']
		output = self.execute('df -hT | grep "/dev"')
		if output:
			for line in output:
				substr_list = re.split('[ ]+',line.strip())
				mem_info_list.append(dict(zip(params_list,substr_list)))	
		return mem_info_list
		
	def get_cpu_state(self):
		if self.protocol == PR_RSH:
			return
		cpu_state_list = []
		output = self.execute('mpstat -P ALL')
		if output:
			lines = output.readlines()
			# здесь параметры лежат в 3 строке 
			params_list = re.split('[ ]+',lines[2].strip()) 
			for line in lines[3:]:
				substr_list = re.split('[ ]+',line.strip())
				cpu_state_list.append(dict(zip(params_list,substr_list)))	
		return cpu_state_list
		
	###################################
	# Команды для цисок
	###################################
	def ping_tracert(self, cmd, ip_addr):
		if not self.protocol == PR_RSH:
			return
		output = self.execute("{0} {1}".format(cmd, ip_addr))
		if output:
			return output

	def ip_route(self, cmd, ip_addr):
		if not self.protocol == PR_RSH:
			return
		cmd = 'sh ip route' if cmd == 'ip_route' else 'sh ipv6 route'
		output = self.execute("{0} {1}".format(cmd, ip_addr))
		if output:
			return output

	def bgp_ip_unicast(self, cmd, ip_addr):
		if not self.protocol == PR_RSH:
			return
		if cmd == 'bgp_ipv4_unicast':
			cmd = 'sh bgp ipv4 unicast'
		else:
			cmd = 'sh bgp ipv6 unicast'
		output = self.execute("{0} {1}".format(cmd, ip_addr))
		if output:
			return output

	def ip_int_brief(self):
		if not self.protocol == PR_RSH:
			return
		int_br_list = []
		output = self.execute("sh ip int br")
		if output:
			lines = re.split('[\n]+',output.strip())
			params_list = re.split('[ ]+',lines[0].strip())
			for line in lines[1:]:
				substr_list = re.split('[ ]+',line.strip())
				int_br_list.append(dict(zip(params_list,substr_list)))
			return int_br_list

	def execute(self, command):
		output = None
		try:
			if self.protocol == PR_SSH and self.is_ssh_connection_active():
				stdin, stdout, stderr = self.ssh.exec_command(command)
				# если поток с ошибками пустой,
				# возвращаем файл с выводом команды
				if not stderr.readlines():
					output = stdout
			elif self.protocol == PR_RSH:
				output = sp.check_output(['rsh', self.host, '-n', '-l',
											self.user, command])
		except Exception as e:
			logging.info( u"Can't execute command:"
							u" '{0}'\n{1}".format(command, str(e)))
		return output

if __name__ == '__main__':
	hm = HostsManager()

	state = hm.connect_to_host('192.168.56.111')
	print state
	#print hm.check_connection_to_hosts()
	
	'''
	s = Server('192.168.56.102','mz', 22, 0)
	s.connect()
	print s.is_ssh_connection_active()
	
	print 'get_cpu_info:'
	print s.get_cpu_info()
	print 'get_ram_info:'
	print s.get_ram_info()
	print 'get_hostname:'
	print s.get_hostname()
	print 'get_mem_info:'
	print s.get_mem_info()
	print 'get_cpu_state:'
	print s.get_cpu_state()
	print 'get_cpu_info:'
	print s.get_cpu_info()
	'''

