# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Telnet provider
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


from base import AbstractProvider


class TelnetClient(AbstractProvider):
    """
    Telnet client
    """
    def __init__(self, address, port):
        super(TelnetClient, self).__init__(address, port)


    def connect(self):
        pass


    def disconnect(self):
        pass


    def __str__(self):
        return "{class_name} {address}:{port}".format(
            class_name=self.__class__.__name__,
            address=self.address,
            port=self.port)