﻿# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# Telegram Bot
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


import logging
import re
import time

import telebot
from telebot import types

import config
from core import server
from lib import bot_strings as bs

t_bot = telebot.TeleBot(config.bot_tocken)
hm = server.HostsManager()

# Настроим формат вывода логов, пока что в консоль
logging.basicConfig(
		format=config.log_format,
		level=logging.INFO)

# костыль, чтоб в случаях реализации пошаговой логики не обрабатывался 
# дефолтный хендлер. (вообще класс, хранящий состояние, бы сделать)
is_handle_need = True


# декоратор для аутентификации пользователей
def auth_check(cmd_handler):
	def wrapper(msg):
		if str(msg.chat.id) not in config.chat_ids:
			t_bot.send_message(msg.chat.id, bs.abort_chat)
			return
		return cmd_handler(msg)
	return wrapper


def get_keyboard_with_commands():
	kb = types.ReplyKeyboardMarkup(resize_keyboard=True)
	kb.add(*[types.KeyboardButton(name) for name in sorted(bs.nix_commands.keys())])
	return kb


# набор обработчиков команд
@t_bot.message_handler(commands=['start'])
def bot_start(message):
	start_msg = bs.hello
	t_bot.send_message(message.chat.id, start_msg)


@t_bot.message_handler(commands=['help'])
@auth_check
def get_help(message):
	for cmd_type_title, commands in bs.commands_dict.items():
		help_msg = u'<b>{}</b>\n'.format(cmd_type_title)
		for command in sorted(commands.keys()):
			help_msg += u'\t{0}<i>{1}</i>\n'.format(command, 
													commands.get(command))
		t_bot.send_message(message.chat.id, help_msg, parse_mode='HTML',
							reply_markup = get_keyboard_with_commands())
	
	
@t_bot.message_handler(commands=['show_servers'])
@auth_check
def show_servers(message):
	ans_msg = hm.check_connection_to_hosts()
	if not ans_msg:
		t_bot.send_message(
			message.chat.id, 
			bs.sh_srv_fail, 
			parse_mode='HTML')
	out_str = u'<b>{}</b>\n'.format(bs.sh_srv_title)
	for key, val in ans_msg.items(): # key - хост, val - состояние
		state = bs.TICK
		if val:
			state = bs.CROSS
		out_str += u'[{0}]: {1}\n'.format(state, key)
	t_bot.send_message(message.chat.id, out_str, parse_mode='HTML')


@t_bot.message_handler(commands=['choose_server'])
@auth_check
def choose_server(message):
	global is_handle_need
	# пока тут в клаву попадают все сервера из hosts
	kb = types.ReplyKeyboardMarkup(resize_keyboard=True)
	kb.add(*[types.KeyboardButton(name) for name in hm.hosts.keys()])
	out_str = u'{0}{1}'.format(bs.ch_srv_list, str(hm.hosts.keys()[0]))
	msg = t_bot.send_message(message.chat.id, out_str, reply_markup=kb)
	t_bot.register_next_step_handler(msg, choose_server_step_2)
	is_handle_need = False


def choose_server_step_2(message):
	global is_handle_need
	if not re.search(bs.RE_IPV4, message.text):
		t_bot.send_message(message.chat.id, bs.ch_srv_wrong_ip)
		return
	is_connected = hm.connect_to_host(message.text)
	out_str = u'{0}{1}'.format(bs.ch_srv_conn, str(message.text))
	if is_connected:
		out_str += bs.ch_srv_conn_success
		t_bot.send_message(
						message.chat.id, 
						out_str, 
						reply_markup = get_keyboard_with_commands())
	else:
		out_str += bs.ch_srv_conn_fail
		t_bot.send_message(message.chat.id, out_str)
	is_handle_need = True


@t_bot.message_handler(commands=['auth_request'])
def auth_request(message):
	out_str = bs.auth_req_usr_ok
	if str(message.chat.id) not in config.chat_ids:
		username = '{0} {1}'.format(message.from_user.first_name, 
									message.from_user.last_name)
		# пока что просто отправим запрос в логи
		logging.info(
				u"{0} requests access to sermon_bot "
				u"from chat {1}".format(username, message.chat.id))
		out_str = bs.auth_req_sent
	t_bot.send_message(message.chat.id, out_str)


###################################
# Команды для *nix машин
###################################
@t_bot.message_handler(commands=['mem_info'])
@auth_check
def mem_info(message):
	ans_msg = hm.srv.get_mem_info()
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.mem_info_fail)
		return
	for dev in ans_msg:
		out_str = u'<b>{0}{1}:</b>\n'.format(bs.mem_info_dev, dev.get('Fs'))
		for key,val in dev.items():
			out_str += u'\t{0}: <i>{1}</i>\n'.format(key,val)
		t_bot.send_message(message.chat.id, out_str, parse_mode='HTML')
		
	
@t_bot.message_handler(commands=['ram_info'])
@auth_check
def ram_info(message):
	ans_msg = hm.srv.get_ram_info()
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.ram_info_fail)
		return
	# r_type - mem/swap, params - список их параметров
	for r_type, params in ans_msg.items(): 
		out_str = u'<b>{}: </b>\n'.format(r_type)
		for key, val in params.items():
			out_str += '\t{0}: <i>{1}</i>\n'.format(key, val)
		t_bot.send_message(message.chat.id, out_str, parse_mode = 'HTML')
		
	
@t_bot.message_handler(commands=['cpu_info'])
@auth_check
def cpu_info(message):
	ans_msg = hm.srv.get_cpu_info()
	if not ans_msg:	
		t_bot.send_message(message.chat.id, bs.cpu_info_fail)
		return
	out_str = u'<b>{}</b>\n'.format(bs.cpu_info_title)
	for key,val in ans_msg.items():
		if key in bs.cpu_info_fields:
			out_str += '\t{0}<i>{1}</i>\n'.format(key, val)
	t_bot.send_message(message.chat.id, out_str, parse_mode='HTML')
	

@t_bot.message_handler(commands=['cpu_state'])
@auth_check
def cpu_state(message):
	ans_msg = hm.srv.get_cpu_state()
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.cpu_state_fail)
		return
	out_str = u'<b>{}</b>\n'.format(bs.cpu_state_title)
	for cpu in ans_msg:
		for key in sorted(cpu.keys()[1:],reverse = True):
			out_str += '\t{0}: <i>{1}</i>\n'.format(key, cpu.get(key))
		out_str += '\n'
	t_bot.send_message(message.chat.id, out_str, parse_mode='HTML')


@t_bot.message_handler(commands=['where_am_i'])
@auth_check
def where_am_i(message):
	ans_msg = hm.srv.get_hostname()
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.where_am_i_fail)
		return
	t_bot.send_message(message.chat.id, ans_msg)


###################################
# Команды для цисок
###################################	
@t_bot.message_handler(commands=['ping', 'traceroute'])
@auth_check
def ping_tracert(message):
	msg = message.text.split(' ')
	#cmd = msg[0][1:]
	cmd = re.search(bs.RE_CMD, msg[0]).group(0)[1:]
	ip_addr = msg[1] if len(msg) > 1 else '8.8.8.8'
	if not re.search(bs.RE_IPV4, ip_addr):
		t_bot.send_message(message.chat.id, bs.ping_wr_ip)
		return
	ans_msg = hm.srv.ping_tracert(cmd, ip_addr)
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.ping_fail)
	else:
		t_bot.send_message(message.chat.id, ans_msg)


@t_bot.message_handler(commands=['ip_route', 'ipv6_route'])
@auth_check
def ip_route(message):
	msg = message.text.split(' ')
	#cmd = msg[0][1:]
	cmd = re.search(bs.RE_CMD, msg[0]).group(0)[1:]
	ip_addr = msg[1] if len(msg) > 1 else ''
	if ((not re.search(bs.RE_IPV4, ip_addr) and cmd == 'ip_route') or 
			(not re.search(bs.RE_IPV6, ip_addr) and cmd == 'ipv6_route')):
		t_bot.send_message(message.chat.id, bs.ip_route_wr_ip)
		return
	ans_msg = hm.srv.ip_route(cmd, ip_addr)
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.ip_route_fail)
	else:
		t_bot.send_message(message.chat.id, ans_msg)


@t_bot.message_handler(commands=['bgp_ipv4_unicast', 'bgp_ipv6_unicast'])
@auth_check
def bgp_ip_unicast(message):
	msg = message.text.split(' ')
	cmd = re.search(bs.RE_CMD, msg[0]).group(0)[1:]
	ip_addr = msg[1] if len(msg) > 1 else ''
	if ((not re.search(bs.RE_IPV4, ip_addr) and cmd == 'bgp_ipv4_unicast') or 
			(not re.search(bs.RE_IPV6, ip_addr) and cmd == 'bgp_ipv6_unicast')):
		t_bot.send_message(message.chat.id, bs.bgp_ip_unicast_wr_ip)
		return
	ans_msg = hm.srv.bgp_ip_unicast(cmd, ip_addr)
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.bgp_ip_unicast_fail)
	else:
		t_bot.send_message(message.chat.id, ans_msg)


@t_bot.message_handler(commands=['ip_int_brief'])
@auth_check
def ip_int_brief(message):
	ans_msg = hm.srv.ip_int_brief()
	if not ans_msg:
		t_bot.send_message(message.chat.id, bs.ip_int_br_fail)
		return
	out_str = u'<b>{}</b>\n'.format(bs.ip_int_br_title)
	for int in sorted(ans_msg, key=lambda x: x['Interface']):
		out_str = ''
		for key, val in sorted(int.items()):
			out_str += '\t{0}: <i>{1}</i>\n'.format(key, val)
		t_bot.send_message(message.chat.id, out_str, parse_mode='HTML')


@t_bot.message_handler(commands=['cat_pls'])
def cat_pls(message):
	t_bot.send_message(
			message.chat.id, (
			u'http://thecatapi.com/api/images/'
			u'get?format=src&type=gif&timestamp={}'.format(time.time())))
	username = '{0} {1}'.format(message.from_user.first_name, 
								message.from_user.last_name)
	logging.info(
			u"{0} requests a cat "
			u"from chat {1}".format(username, message.chat.id))

	
# дефолтный обработчик при получении неизвестной команды	
@t_bot.message_handler(func=lambda message: True)
def echo_all(message):
	global is_handle_need
	if is_handle_need:
		t_bot.reply_to(message, bs.echo_all_unknown_cmd)

	
if __name__ == '__main__':
	# пошлем админу сообщение о ребуте
	t_bot.send_message(87101023, u'Йа сломалсо! Глянь логи')
	t_bot.polling()
