# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------
# All strings for scripts
# -----------------------------------------------------------------------
# Copyright (C) 2017-2017 The Sermon Bot
# -----------------------------------------------------------------------
# This file is part of The Sermon Bot.
#
# The Sermon Bot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Sermon Bot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with The Sermon Bot.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


# юникод эмоджи символов
TICK = u'\U00002716'
CROSS = u'\U00002714'
BOWING = u'\U0001F647'
OK = u'\U0001F44C'

# регулярки для IP стащил отсюда https://gist.github.com/mnordhoff/2213179
RE_IPV4 = '^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
RE_IPV6 = '^(?:(?:[0-9A-Fa-f]{1,4}:){6}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|::(?:[0-9A-Fa-f]{1,4}:){5}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,4}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){,6}[0-9A-Fa-f]{1,4})?::)$'
RE_CMD = '\/[\w\d_]+'

global_commands = {
	u'/show_servers': u' - Посмотреть доступность серверов',
	u'/choose_server': u' - Выбрать сервер'}

nix_commands = {
	u'/mem_info': u' - Информация о дисковом пространстве',
	u'/ram_info': u' - Информация о памяти сервера',
	u'/cpu_info': u' - Информация о CPU',
	u'/cpu_state': u' - Информация о загрузке CPU',
	u'/where_am_i': u' - Посмотреть подключенный хост'}

cisco_commands = {
	u'/ping X.X.X.X': u' - Пинг до X.X.X.X',
	u'/traceroute X.X.X.X': u' - Маршрут до X.X.X.X',
	u'/ip_int_brief': u' - Состояние интерфейсов',
	u'/ip_route X.X.X.X': u' - Запись в ТМ о X.X.X.X',
	u'/ipv6_route X::X': u' - Запись в ТМ о X::X',
	u'/bgp_ipv4_unicast X.X.X.X': u' - Cостояние bgp до X.X.X.X',
	u'/bgp_ipv6_unicast X::X': u' - Cостояние bgp до X::X'}
	
commands_dict = {
	u'Общие команды:': global_commands,
	u'Команды для цисок:': cisco_commands,
	u'Команды для *nix машин:': nix_commands}
			
# строки для метода bot_start
hello = (
	u'Привет! '
	u'Этот бот поможет тебе мониторить состояние серверов. '
	u'Набери /help что бы получить список команд.')
	
# строки для метода get_help	
get_help_cmd_list = u'Список доступных команд:'

# строки для метода show_servers
sh_srv_title = u'Доступность серверов на данный момент:'
sh_srv_fail = (
	u'Эм, почему то не могу проверить состояние серверов. '
	u'Может повторить позже?')
	
# строки для метода choose_server			
ch_srv_list = u'Выбери сервер из списка доступных, например '
ch_srv_conn = u'Подключение к '
ch_srv_conn_success = u' прошло успешно.'
ch_srv_conn_fail = (u' прошло не совсем успешно. '
					u'Может попробовать еще раз? /choose_server')
ch_srv_wrong_ip = (
	u'Некорректный формат IP-адреса. '
	u'Попробуй снова /choose_server')

# строки для метода mem_info
mem_info_dev = u'Устройство '
mem_info_fail = (
	u'Не могу получить информацию о дисковом пространстве, '
	u'может стоит проверить подключение?')

# строки для метода mem_info
ram_info_fail = (
	u'Не могу получить информацию о памяти, '
	u'может стоит проверить подключение?')

# строки для метода cpu_info
cpu_info_title = u'Информация о CPU: '
cpu_info_fail = (
	u'Не могу получить информацию о CPU, '
	u'может стоит проверить подключение?')
cpu_info_fields = [
	'vendor_id','processor','model name',
	'cpu MHz','cache size','address sizes',
	'bogomips']

# строки для метода cpu_state
cpu_state_title = u'Информация о загрузке CPU: '
cpu_state_fail = (
	u'Не могу получить информацию о загрузке CPU, '
	u'может стоит проверить подключение?')

# строки для метода where_am_i
where_am_i_fail = u'Ты ни к чему не подключен. Попробуй /choose_server'

# строки для метода echo_all
echo_all_unknown_cmd = u'Такой команды я пока не знаю, может глянешь /help ?'

# строки для метода auth_request
auth_req_sent = (
	u'Запрос на предоставление доступа отправлен. '
	u'Осталось немного подождать.. {}'.format(BOWING))
auth_req_usr_ok = (
	u'Ты уже и так имеешь доступ '
	u'ко всему, что я умею. {}'.format(OK))

# строки для декоратора auth_check
abort_chat = (
	u'Кажется, я не знаю тебя. Попробуй обратиться к админу, '
	u'чтобы получить доступ к моему функционалу. Для этого выполни '
	u'команду /auth_request')
	
# строки для метода ping_tracert
ping_fail = (
	u'Не могу выполнить ping (traceroute). '
	u'Нет ответа от удаленного сервера.')
ping_wr_ip = (
	u'Некорректный формат IP-адреса. '
	u'Попробуй снова, например, /ping 8.8.8.8')

# строки для метода ip_route
ip_route_fail = (
	u'Не могу выполнить sh ip route. '
	u'Нет ответа от удаленного сервера.')
ip_route_wr_ip = (
	u'Некорректный формат IP-адреса. '
	u'Попробуй снова, например, /ip_route 8.8.8.8')		
	
# строки для метода ip_int_br
ip_int_br_fail = (
	u'Не могу выполнить "sh ip int br". '
	u'Нет ответа от удаленного сервера.')
ip_int_br_title = u'Состояние интерфейсов:'

# строки для метода bgp_ip_unicast
bgp_ip_unicast_fail = (
	u'Не могу выполнить "sh bgp ip unicast". '
	u'Нет ответа от удаленного сервера.')
bgp_ip_unicast_wr_ip = (
	u'Некорректный формат IP-адреса. '
	u'Попробуй снова, например, /bgp_ipv4_unicast 8.8.8.8')	